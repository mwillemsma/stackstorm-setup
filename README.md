### AIO stackstorm docker

### Getting started

Deploy playbook to get all in one stackstorm in docker.

```bash
ansible-playbook setup-stackstorm.yml --diff
```

### Packs

The following packs are installed and configured ready to be used.

- slack
- jira

### Deploy all the things with ansible playbook


Building blocks are versioned in git repo

contains app deployment templates
contains jinja templates
contains workspace


Tshoot sensors - slack

https://docs.stackstorm.com/troubleshooting/sensors.html

known issue with RTM json payload https://github.com/StackStorm-Exchange/stackstorm-slack/issues/50
NEEDS PATCHING :X
